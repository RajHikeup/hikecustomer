﻿using System;
using Xamarin.Forms;

namespace HikeCustomer.ViewModel
{
    public class EnterSaleViewModel : BaseModel 
    {
        public EnterSaleViewModel()
        {

        }


        private void  GetMessage()
        {
            MessagingCenter.Send<MessageCenterSubscribeClass>(new MessageCenterSubscribeClass(), "CartDataReceived");
        }
    }
}
