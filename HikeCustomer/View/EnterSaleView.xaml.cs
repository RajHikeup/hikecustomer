﻿using System;
using System.Collections.Generic;
using HikeCustomer.Helper;
using HikeCustomer.Model;
using Xamarin.Forms;

namespace HikeCustomer.View
{
    public partial class EnterSaleView : ContentPage
    {
        private IPeerComunicator print;

        public EnterSaleView()
        {
            InitializeComponent();

        }

        void Button_Clicked(System.Object sender, System.EventArgs e)
        {

            MessageLabel.Text = MessageEditor.Text;

            //IEmailComposer emailComposer = DependencyService.Get<IEmailComposer>();


            //print.StartPeerConnection("iPadJan");

            print.SendInvoiceMessage("propprop");

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            print = DependencyService.Get<IPeerComunicator>();
            print.ReceivePeerData(Fiska_PaymentSuccessed);

        }

        private async void Fiska_PaymentSuccessed(PeerResponse response)
        {
            
        }
    }
}
