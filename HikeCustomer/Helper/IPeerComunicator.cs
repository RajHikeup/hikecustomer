﻿using System;
using HikeCustomer.Model;

namespace HikeCustomer.Helper
{
    public interface IPeerComunicator
    {
        void StartPeerConnection(string StoreID);

        void ReceivePeerData(Action<PeerResponse> peerActionResponse);
        void SendInvoiceMessage(string invoice);

    }
}
