﻿using System;
using System.Diagnostics;
using Foundation;
using HikeCustomer.Helper;
using HikeCustomer.iOS.Helper;
using HikeCustomer.Model;
using HikeCustomer.ViewModel;
using MultipeerConnectivity;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(PeerComunicator))]
namespace HikeCustomer.iOS.Helper
{
    public class PeerComunicator : IPeerComunicator
    {


        public static Action<PeerResponse> PeerResponse;


        static bool UserInterfaceIdiomIsPhone
        {
            get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
        }

        MCNearbyServiceAdvertiser mcNearbyServiceAdvertiser;

        MCNearbyServiceBrowser mcNearbyServiceBrowser;


        MCPeerID mcPeerID;
        public static MCSession mcSession;

        string ServiceType = "example-color";

        public ViewController ViewController;

        MyNearbyServiceAdvertiserDelegate myNearbyServiceAdvertiserDelegate = new MyNearbyServiceAdvertiserDelegate();
        MYNearbyServiceBrowserDelegate myNearbyServiceBrowserDelegate = new MYNearbyServiceBrowserDelegate();
        MySessionDelegate mySessionDelegate = new MySessionDelegate();


        public PeerComunicator()
        {

            mcPeerID = new MCPeerID(UIDevice.CurrentDevice.Name);
            mcSession = new MCSession(mcPeerID);
            mcSession.Delegate = mySessionDelegate;
            //mySessionDelegate.sessionVC = this;


            //var tempDictionary = new NSMutableDictionary();
            //var key = new NSString("Tempkey");
            //var value = new NSString("TempValue");

            //tempDictionary.Add(key, value);


            mcNearbyServiceAdvertiser = new MCNearbyServiceAdvertiser(mcPeerID, null, ServiceType);
            mcNearbyServiceBrowser = new MCNearbyServiceBrowser(mcPeerID, ServiceType);

            //myNearbyServiceAdvertiserDelegate.sessionVC = this;

            mcNearbyServiceAdvertiser.Delegate = myNearbyServiceAdvertiserDelegate;
            mcNearbyServiceAdvertiser.StartAdvertisingPeer();

            mcNearbyServiceBrowser.Delegate = myNearbyServiceBrowserDelegate;
            mcNearbyServiceBrowser.StartBrowsingForPeers();

            //myNearbyServiceBrowserDelegate.sessionVC = this;
        }

        public void StartPeerConnection(string StoreID)
        {
            //throw new NotImplementedException();

        }



        public void SendInvoiceMessage(string invoice)
        {
            try
            {
                invoice = "temprory invoices....";
                NSError error;
                var s = mcSession.ConnectedPeers.Length;

                mcSession.SendData(invoice, mcSession.ConnectedPeers, MCSessionSendDataMode.Reliable, out error);
            }
            catch (Exception ex)
            {

            }
        }

        public void ReceivePeerData(Action<PeerResponse> peerActionResponse)
        {
            peerActionResponse = PeerResponse;
        }

    }
        public class MyNearbyServiceAdvertiserDelegate : MCNearbyServiceAdvertiserDelegate
        {




            public override void DidReceiveInvitationFromPeer(MCNearbyServiceAdvertiser advertiser, MCPeerID peerID, NSData context, MCNearbyServiceAdvertiserInvitationHandler invitationHandler)
            {
                invitationHandler(true, PeerComunicator.mcSession);


            }

            public override void DidNotStartAdvertisingPeer(MCNearbyServiceAdvertiser advertiser, NSError error)
            {
                //
            }


        }



        public class MYNearbyServiceBrowserDelegate : MCNearbyServiceBrowserDelegate
        {

            public ViewController sessionVC;
            public override void FoundPeer(MCNearbyServiceBrowser browser, MCPeerID peerID, NSDictionary info)
            {

                browser.InvitePeer(peerID, PeerComunicator.mcSession, null, 30);
                // throw new NotImplementedException();
            }

            public override void LostPeer(MCNearbyServiceBrowser browser, MCPeerID peerID)
            {
                //throw new NotImplementedException();
            }
        }





        class MySessionDelegate : MCSessionDelegate
        {

        //public MCNearViewController sessionVC;
        


            public override void DidChangeState(MCSession session, MCPeerID peerID, MCSessionState state)
            {
                switch (state)
                {
                    case MCSessionState.Connected:
                        Console.WriteLine("Connected: {0}", peerID.DisplayName);
                        break;
                    case MCSessionState.Connecting:
                        Console.WriteLine("Connecting: {0}", peerID.DisplayName);
                        break;
                    case MCSessionState.NotConnected:
                        Console.WriteLine("Not Connected: {0}", peerID.DisplayName);
                        break;
                }
            }

            public override void DidReceiveData(MCSession session, NSData data, MCPeerID peerID)
            {
                PeerResponse peerResponse = new PeerResponse();

                InvokeOnMainThread(() =>
                {
                    var alert = new UIAlertView("", data.ToString(), null, "OK");
                    //alert.Show();


                    if (data != null)
                    {
                        MessagingCenter.Send<MessageCenterSubscribeClass>(new MessageCenterSubscribeClass(), data.ToString());
                        peerResponse.Data = data.ToString();

                    }
                    else
                    {
                        Debug.WriteLine("DATA IS NULL");
                        peerResponse.Data = ("DATA IS NULL");
                    }

                    
                    peerActionResponse?.Invoke(PeerComunicator.PeerResponse);
                    //sessionVC.SetColor(data.ToString());
                });
            }

            public override void DidStartReceivingResource(MCSession session, string resourceName, MCPeerID fromPeer, NSProgress progress)
            {
            }

            public override void DidFinishReceivingResource(MCSession session, string resourceName, MCPeerID formPeer, NSUrl localUrl, NSError error)
            {
                error = null;
            }

            public override void DidReceiveStream(MCSession session, NSInputStream stream, string streamName, MCPeerID peerID)
            {
            }
        }
    

}
