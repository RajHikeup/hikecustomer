﻿using System;
using Foundation;
using MultipeerConnectivity;
using UIKit;

namespace HikeCustomer.iOS.Helper
{
  

	public partial class ViewController : UIViewController
	{

		float volume = 0.5f;
		float pitch = 1.0f;


		static bool UserInterfaceIdiomIsPhone
		{
			get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
		}

		public ViewController(IntPtr handle) : base(handle)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.
			if (!UserInterfaceIdiomIsPhone)
			{
                //openMultipeerBrowser.TouchUpInside += (sender, e) =>
                //{
                //    StartMultipeerBrowser();
                //};

                //SendMessageButton.TouchUpInside += (sender, e) =>
                //{
                //    SendMessage();
                //};
            }
			else
			{
				StartMultipeerAdvertiser();
			}

			sessionDelegate.myViewController = this;

		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}

		#region Multipeer Connectivity Demo
		MCSession session;
		MCPeerID peer;
		MCBrowserViewController browser;
		MCAdvertiserAssistant assistant;
		MySessionDelegate sessionDelegate = new MySessionDelegate();
		MyBrowserDelegate browserDelegate = new MyBrowserDelegate();
		NSDictionary dict = new NSDictionary();
		static readonly string serviceType = "room2";



		void StartMultipeerAdvertiser()
		{
			peer = new MCPeerID("raj123");
			session = new MCSession(peer);
			session.Delegate = sessionDelegate;
			assistant = new MCAdvertiserAssistant(serviceType, dict, session);
			assistant.Start();
		}

		void StartMultipeerBrowser()
		{
			peer = new MCPeerID("raj123");
			session = new MCSession(peer);
			session.Delegate = sessionDelegate;
			browser = new MCBrowserViewController(serviceType, session);
			browser.Delegate = browserDelegate;
			browser.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;
			this.PresentViewController(browser, true, null);
		}

		void SendMessage()
		{
            //var msg1 = MessageTextbox.Text;
            var message = NSData.FromString(
                String.Format("{0} found the monkey", peer.DisplayName));
            NSError error;
			session.SendData(message, session.ConnectedPeers,
				MCSessionSendDataMode.Reliable, out error);
		}

		class MySessionDelegate : MCSessionDelegate
		{
			public ViewController myViewController;
			public UITextView MessageTextbox;
			public override void DidChangeState(MCSession session, MCPeerID peerID, MCSessionState state)
			{
				switch (state)
				{
					case MCSessionState.Connected:
						Console.WriteLine("Connected: {0}", peerID.DisplayName);
						break;
					case MCSessionState.Connecting:
						Console.WriteLine("Connecting: {0}", peerID.DisplayName);
						break;
					case MCSessionState.NotConnected:
						Console.WriteLine("Not Connected: {0}", peerID.DisplayName);
						break;
				}
			}

			public override void DidReceiveData(MCSession session, NSData data, MCPeerID peerID)
			{
				InvokeOnMainThread(() => {
					//myViewController.MessageLabel.Text = data.ToString();
                    var alert = new UIAlertView("", data.ToString(), null, "OK");
                    alert.Show();
                });


			}

			public override void DidStartReceivingResource(MCSession session, string resourceName, MCPeerID fromPeer, NSProgress progress)
			{
			}

			public override void DidFinishReceivingResource(MCSession session, string resourceName, MCPeerID formPeer, NSUrl localUrl, NSError error)
			{
				error = null;
			}

			public override void DidReceiveStream(MCSession session, NSInputStream stream, string streamName, MCPeerID peerID)
			{
			}
		}

		class MyBrowserDelegate : MCBrowserViewControllerDelegate
		{
			public override void DidFinish(MCBrowserViewController browserViewController)
			{
				InvokeOnMainThread(() => {
					browserViewController.DismissViewController(true, null);
				});
			}

			public override void WasCancelled(MCBrowserViewController browserViewController)
			{
				InvokeOnMainThread(() => {
					browserViewController.DismissViewController(true, null);
				});
			}
		}
		#endregion

	}
}
